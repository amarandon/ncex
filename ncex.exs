defmodule NcEx do
  # tag for list of dimensions
  @nc_dimension "\x00\x00\x00\x0A"
  # tag for list of variables
  @nc_variable "\x00\x00\x00\x0B"
  # tag for list of attributes
  @nc_attribute "\x00\x00\x00\x0C"

  def parse(<<"CDF\x01", tail::binary>> = data) when is_binary(data) do
    parse_header(tail)
  end

  def parse(_) do
    {:error, "Not a valid NetCDF file"}
  end

  def parse_header(<<numrecs::size(32), tail::binary>>) do
    result = %{numrecs: numrecs, dimensions: %{}, attributes: %{}, variables: []}

    {result, tail} = parse_dim_list(result, tail)
    {result, tail} = parse_gatt_list(result, tail)
    parse_var_list(result, tail)
  end

  def parse_dim_list(result, <<@nc_dimension, nelems::size(32), tail::binary>>) do
    result = Map.put(result, :dim_count, nelems)
    {result, tail} = parse_next_dim(result, tail, nelems)
    {result, tail}
  end

  def parse_next_dim(result, tail, 0), do: {result, tail}

  def parse_next_dim(
        result,
        <<name_length::size(32), name::bytes-size(name_length), tail::binary>>,
        nelems
      ) do
    tail = skip_padding(name_length, tail)
    <<dimension_size::size(32), tail::binary>> = tail
    result = put_in(result[:dimensions][name], dimension_size)
    parse_next_dim(result, tail, nelems - 1)
  end

  def parse_gatt_list(result, <<@nc_attribute, nelems::size(32), tail::binary>>) do
    result = Map.put(result, :gatt_count, nelems)
    {result, tail} = parse_next_gatt(result, tail, nelems)
    {result, tail}
  end

  def parse_next_gatt(result, tail, 0), do: {result, tail}

  def parse_next_gatt(
        result,
        <<name_length::size(32), name::bytes-size(name_length), tail::binary>>,
        nelems
      ) do
    tail = skip_padding(name_length, tail)
    {value, tail} = parse_gatt_value(tail)
    result = put_in(result[:attributes][name], value)
    parse_next_gatt(result, tail, nelems - 1)
  end

  def parse_gatt_value(
        <<"\x00\x00\x00\x02", value_length::size(32), value::bytes-size(value_length),
          tail::binary>>
      ) do
    tail = skip_padding(value_length, tail)
    {value, tail}
  end

  def parse_gatt_value(<<"\x00\x00\x00\x04", 1::size(32), value::size(32), tail::binary>>) do
    {value, tail}
  end

  def parse_var_list(result, <<@nc_variable, nelems::size(32), tail::binary>>) do
    result = Map.put(result, :var_count, nelems)
    {result, tail} = parse_next_var(result, tail, nelems)
    {result, tail}
  end

  def parse_next_var(result, tail, 0), do: {result, tail}

  def parse_next_var(
        result,
        <<name_length::size(32), name::bytes-size(name_length), tail::binary>>,
        nelems
      ) do
    tail = skip_padding(name_length, tail)
    <<dimensionality::size(32), dimid::size(32), tail::binary>> = tail
    IO.inspect({"***", name, dimensionality, dimid})
    {result, tail}
  end

  def skip_padding(value_length, tail) do
    value_padding = compute_padding(value_length)
    <<_::bytes-size(value_padding), tail::binary>> = tail
    tail
  end

  def compute_padding(n) do
    case rem(n, 4) do
      0 -> 0
      remainder -> 4 - remainder
    end
  end
end

filename = "W_fr-meteofrance,MODEL,ENSEMBLE+FORECAST+SURFACE+PM10+0H24H_C_LFPW_20190929000000.nc"
data = File.read!(filename)
IO.inspect(NcEx.parse(data))
# data = File.read!("/etc/passwd")
# IO.inspect(NcEx.parse(data))
